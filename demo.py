import numpy as np
from sklearn.svm import SVR
# from sklearn.metrics import explained_variance_score
# import matplotlib.pyplot as plt
import requests
import json
# plt.switch_backend('macosx')  

# gets data
def get_data(symbol):

	# creates lists
	dates = []
	prices = []

	half_url = 'https://api.iextrading.com/1.0/stock/%s/chart/1y' % symbol
	api_parameters = {'filter': 'date,open'}
	resp = requests.get(half_url, params=api_parameters)
	data = resp.json()

	for value in data:
		dates.append(len(prices))
		prices.append(float(value['open']))

	return dates, prices
	
# predicts price
def predict_price(x):

	 # converting to matrix of n X 1
	time = np.reshape(dates,(len(dates), 1))

	# initialize model
	svr_rbf = SVR(kernel= 'rbf', C=1e4,gamma=1e-6)

	# fit model
	svr_rbf.fit(time, prices) 
	
	# accuracy of model using data set
	# acc_rbf = explained_variance_score(prices, svr_rbf.predict(time)) * 100.0

#	plt.scatter(dates, prices, color= 'black', label= 'Data') # plotting the initial datapoints 
#	plt.plot(dates, svr_rbf.predict(dates), color= 'red', label= 'RBF model') # plotting the line made by the RBF kernel
#	plt.xlabel('Date')
#	plt.ylabel('Price')
#	plt.title('Support Vector Regression')
#	plt.legend()
#	plt.show()

	pred = svr_rbf.predict(x)[0]

	return pred




# enter company symbol
company = input('Enter company symbol: ')

# gets data
dates, prices = get_data(company)

# predicts price
prediction = predict_price(len(prices))

# displays prediction
print(prediction)